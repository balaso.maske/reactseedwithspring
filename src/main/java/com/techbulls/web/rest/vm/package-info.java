/**
 * View Models used by Spring MVC REST controllers.
 */
package com.techbulls.web.rest.vm;
