package com.techbulls.web.rest.comment;

import com.codahale.metrics.annotation.Timed;
import com.techbulls.domain.Comment;
import com.techbulls.service.comment.CommentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.net.URISyntaxException;

@RestController
@RequestMapping("/api/comment")
public class CommentResource {
    private final Logger log = LoggerFactory.getLogger(CommentResource.class);

    @Autowired
    private CommentService commentService;

    @PostMapping("")
    @Timed
    public Comment createComment(@Valid @RequestBody Comment comment) throws URISyntaxException {
        log.debug("request to save Comment : {}", comment);
        System.out.println("comment "+comment);

        commentService.saveComment(comment);
/*
            return ResponseEntity.created(new URI("/api/users/" + newUser.getLogin()))
                .headers(HeaderUtil.createAlert( "userManagement.created", newUser.getLogin()))
                .body(newUser);*/
        return comment;
    }
}
