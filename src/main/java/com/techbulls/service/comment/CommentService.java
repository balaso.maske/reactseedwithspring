package com.techbulls.service.comment;

import com.codahale.metrics.annotation.Timed;
import com.techbulls.domain.Comment;
import com.techbulls.repository.AuthorityRepository;
import com.techbulls.repository.comment.CommentRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.net.URISyntaxException;

@Service
@Transactional
public class CommentService {

    private final Logger log = LoggerFactory.getLogger(CommentService.class);

    @Autowired
    private CommentRepository commentRepository;

    private AuthorityRepository authorityRepository;

    public Comment saveComment(Comment comment){
        log.debug("Save Comment: {}", comment);
        commentRepository.save(comment);
        return comment;
    }

}
