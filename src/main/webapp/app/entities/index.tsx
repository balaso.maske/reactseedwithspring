import React from 'react';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import CommentManagement from './comment-management';
import CommentManagementUpdate from './comment-management/comment-management-update';

const Routes = ({ match }) => (
  <div>
    <ErrorBoundaryRoute path={`${match.url}/commentManagement`} component={CommentManagement} />
  </div>
);

export default Routes;
