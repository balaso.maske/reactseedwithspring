import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Label, Row, Col } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvField, AvFeedback } from 'availity-reactstrap-validation';
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { locales, languages } from 'app/config/translation';
import { findAllMOCKCOMMENTS, getComment, createComment, updateComment } from './comment-management.reducer';
import { IRootState } from 'app/shared/reducers';

export interface ICommentManagementUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ login: string }> {}

export interface ICommentManagementUpdateState {
  isNew: boolean;
}
export class CommentManagementUpdate extends React.Component<ICommentManagementUpdateProps, ICommentManagementUpdateState> {
  state: ICommentManagementUpdateState = {
    isNew: !this.props.match.params || !this.props.match.params.login
  };

  componentDidMount() {
    /*!this.state.isNew && this.props.getUser(this.props.match.params.login);
    this.props.getRoles();*/
  }

  componentWillUnmount() {
    /* this.props.reset();*/
  }

  saveComment = (event, values) => {
    if (this.state.isNew) {
      this.props.createComment(values);
    } else {
      this.props.updateComment(values);
    }
    this.handleClose();
  };

  handleClose = () => {
    this.props.history.push('/entity/');
  };

  render() {
    const isInvalid = false;
    const { comment, loading, updating } = this.props;
    const { isNew } = this.state;
    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h1>Create or edit a Comment</h1>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm onValidSubmit={this.saveComment}>
                {comment.id ? (
                  <AvGroup>
                    <Label for="id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvField type="text" className="form-control" name="id" required readOnly value={comment.id} />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label for="login">Name</Label>
                  <AvField
                    type="text"
                    className="form-control"
                    name="name"
                    validate={{
                      required: {
                        value: true,
                        errorMessage: translate('register.messages.validate.login.required')
                      },
                      pattern: {
                        value: '^[_.@A-Za-z0-9-]*$',
                        errorMessage: translate('register.messages.validate.login.pattern')
                      },
                      minLength: {
                        value: 1,
                        errorMessage: translate('register.messages.validate.login.minlength')
                      },
                      maxLength: {
                        value: 50,
                        errorMessage: translate('register.messages.validate.login.maxlength')
                      }
                    }}
                    value={comment.name}
                  />
                </AvGroup>
                <AvGroup>
                  <AvField
                    name="email"
                    label={translate('global.form.email')}
                    placeholder={translate('global.form.email.placeholder')}
                    type="email"
                    validate={{
                      required: {
                        value: true,
                        errorMessage: translate('global.messages.validate.email.required')
                      },
                      email: {
                        errorMessage: translate('global.messages.validate.email.invalid')
                      },
                      minLength: {
                        value: 5,
                        errorMessage: translate('global.messages.validate.email.minlength')
                      },
                      maxLength: {
                        value: 254,
                        errorMessage: translate('global.messages.validate.email.maxlength')
                      }
                    }}
                    value={comment.email}
                  />
                </AvGroup>
                <AvGroup>
                  <Label for="body">Body</Label>
                  <AvField
                    label={translate('global.form.body')}
                    type="text"
                    className="form-control"
                    name="body"
                    placeholder="Comment Body"
                    validate={{
                      required: {
                        value: true,
                        errorMessage: translate('global.messages.validate.body.required')
                      },
                      minLength: {
                        value: 5,
                        errorMessage: translate('global.messages.validate.body.minlength')
                      }
                    }}
                    value={comment.body}
                  />
                  <AvFeedback>This field have min 5 characters.</AvFeedback>
                </AvGroup>
                <Button tag={Link} to="/entity/commentManagement" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" type="submit" disabled={isInvalid || updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  comment: storeState.commentManagement.comment,
  loading: storeState.commentManagement.loading,
  updating: storeState.commentManagement.updating
});

const mapDispatchToProps = { findAllMOCKCOMMENTS, getComment, createComment, updateComment };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CommentManagementUpdate);
