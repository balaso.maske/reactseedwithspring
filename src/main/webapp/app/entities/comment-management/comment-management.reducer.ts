import axios from 'axios';

import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';
import { IComments, defaultValue } from 'app/shared/model/comments.model';

export const ACTION_TYPES = {
  FIND_ALL_MOCK_COMMENTS: 'commentManagement/findAllMOCKCOMMENTS',
  FETCH_COMMENT: 'commentManagement/getComment',
  CREATE_COMMENT: 'commentManagement/createComment',
  UPDATE_COMMENT: 'commentManagement/updateComment',
  DELETE_COMMENT: 'commentManagement/deleteComment'
};

const initialState = {
  loading: false,
  errorMessage: null,
  comments: [] as ReadonlyArray<IComments>,
  comment: defaultValue,
  updating: false,
  updateSuccess: false,
  totalItems: 0
};

export type CommentManagementState = Readonly<typeof initialState>;

// Reducer
export default (state: CommentManagementState = initialState, action): CommentManagementState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FIND_ALL_MOCK_COMMENTS):
    case REQUEST(ACTION_TYPES.FETCH_COMMENT):
    case REQUEST(ACTION_TYPES.CREATE_COMMENT):
    case REQUEST(ACTION_TYPES.UPDATE_COMMENT):
    case REQUEST(ACTION_TYPES.DELETE_COMMENT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case FAILURE(ACTION_TYPES.FIND_ALL_MOCK_COMMENTS):
    case FAILURE(ACTION_TYPES.FETCH_COMMENT):
      return {
        ...state,
        loading: false
      };
    case SUCCESS(ACTION_TYPES.FIND_ALL_MOCK_COMMENTS):
      return {
        ...state,
        loading: false,
        comments: action.payload.data,
        totalItems: action.payload.data.length
      };
    case SUCCESS(ACTION_TYPES.CREATE_COMMENT):
    case SUCCESS(ACTION_TYPES.UPDATE_COMMENT):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        comment: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_COMMENT):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        comment: {}
      };
    case SUCCESS(ACTION_TYPES.FETCH_COMMENT):
      return {
        ...state,
        loading: false,
        comment: action.payload.data,
        totalItems: action.payload.headers['x-total-count']
      };
    default:
      return state;
  }
};

const apiUrl = 'api/comment';

export const findAllMOCKCOMMENTS = () => ({
  type: ACTION_TYPES.FIND_ALL_MOCK_COMMENTS,
  payload: axios.get('https://jsonplaceholder.typicode.com/comments')
});

export const getComment: ICrudGetAction<IComments> = id => {
  const requestUrl = `https://jsonplaceholder.typicode.com/comments/${id}`;
  return {
    type: ACTION_TYPES.FETCH_COMMENT,
    payload: axios.get<IComments>(requestUrl)
  };
};

export const createComment: ICrudPutAction<IComments> = comment => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_COMMENT,
    payload: axios.post(apiUrl, comment)
  });
  dispatch(findAllMOCKCOMMENTS());
  return result;
};

export const updateComment: ICrudPutAction<IComments> = comment => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_COMMENT,
    payload: axios.put(apiUrl, comment)
  });
  dispatch(findAllMOCKCOMMENTS());
  return result;
};
