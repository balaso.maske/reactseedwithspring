import React, { Component } from 'react';
import { IRootState } from 'app/shared/reducers';
import { Link, RouteComponentProps } from 'react-router-dom';
import { connect } from 'react-redux';
import { findAllMOCKCOMMENTS } from './comment-management.reducer';
import { Button, Label, Row, Col, Table, Badge } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvField, AvFeedback } from 'availity-reactstrap-validation';
import {
  Translate,
  ICrudGetAllAction,
  ICrudPutAction,
  TextFormat,
  JhiPagination,
  getPaginationItemsNumber,
  getSortState,
  IPaginationBaseState
} from 'react-jhipster';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export interface ICommentManagement extends StateProps, DispatchProps, RouteComponentProps<{}> {}

export class CommentManagement extends React.Component<ICommentManagement, IPaginationBaseState> {
  state: IPaginationBaseState = {
    ...getSortState(this.props.location, ITEMS_PER_PAGE)
  };
  componentDidMount() {
    this.getComments();
  }
  sort = prop => () => {
    this.setState(
      {
        order: this.state.order === 'asc' ? 'desc' : 'asc',
        sort: prop
      },
      () => this.sortComments()
    );
  };
  sortComments() {
    this.props.findAllMOCKCOMMENTS();
    this.props.history.push(`${this.props.location.pathname}?page=${this.state.activePage}&sort=${this.state.sort},${this.state.order}`);
  }

  getComments = () => {
    const { activePage, itemsPerPage, sort, order, search } = this.state;
    /*this.props.findAllMOCKCOMMENTS(activePage - 1, itemsPerPage, `${sort},${order}`);*/
    this.props.findAllMOCKCOMMENTS();
  };

  editComment = comment => () => {
    console.log(comment);
  };

  handlePagination = activePage => this.setState({ activePage }, () => this.sortComments());

  render() {
    const { comments, match, totalItems } = this.props;
    return (
      <div>
        <h2 id="comment-management-page-heading">
          {/* <AvGroup>
            <Label for="Search">Search</Label>
            <AvField
              type="text"
              className="form-control"
              name="name"
              value={this.state.search}
            />
          </AvGroup>*/}
          {/* <Translate contentKey="commentManagement.home.title">Comment</Translate>*/}
          Comment
          <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity">
            <FontAwesomeIcon icon="plus" /> {/*<Translate contentKey="commentManagement.home.createLabel">Create a new POST</Translate>*/}
            Create a new Comment {totalItems}
          </Link>
        </h2>
        <Table responsive striped>
          <thead>
            <tr>
              {/*<th className="hand">
                <Translate contentKey="global.field.id">ID</Translate>
                <FontAwesomeIcon icon="sort" />
              </th>*/}
              <th className="hand">
                {/*<Translate contentKey="global.field.email">email</Translate>*/}
                name
                <FontAwesomeIcon icon="sort" />
              </th>
              <th className="hand">
                {/*<Translate contentKey="global.field.email">email</Translate>*/}
                email
                <FontAwesomeIcon icon="sort" />
              </th>
              <th className="hand">
                {/*<Translate contentKey="global.field.email">email</Translate>*/}
                body
                <FontAwesomeIcon icon="sort" />
              </th>
            </tr>
          </thead>
          <tbody>
            {comments.map((comment, i) => (
              <tr id={comment.id} key={`comment-${i}`} onDoubleClick={this.editComment(comment)}>
                {/*<td>{comment.id}</td>*/}
                <td>{comment.name}</td>
                <td>{comment.email}</td>
                <td>{comment.body}</td>
              </tr>
            ))}
          </tbody>
        </Table>
        <Row className="justify-content-center">
          <JhiPagination
            items={getPaginationItemsNumber(totalItems, this.state.itemsPerPage)}
            activePage={this.state.activePage}
            onSelect={this.handlePagination}
            maxButtons={5}
          />
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  comments: storeState.commentManagement.comments,
  totalItems: storeState.commentManagement.totalItems
});

const mapDispatchToProps = { findAllMOCKCOMMENTS };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CommentManagement);
