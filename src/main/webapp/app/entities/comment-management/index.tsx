import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';
import CommentManagement from './table';
/*import UserManagementDetail from './';*/
import CommentManagementUpdate from './comment-management-update';
/*import UserManagementDeleteDialog from './user-management-delete-dialog';*/

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={CommentManagementUpdate} />
      <ErrorBoundaryRoute path={match.url} component={CommentManagement} />
    </Switch>
  </>
);

export default Routes;
