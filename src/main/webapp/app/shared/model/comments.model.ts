export interface IComments {
  postId?: any;
  id?: any;
  name?: string;
  email?: string;
  body?: string;
}

export const defaultValue: Readonly<IComments> = {
  postId: null,
  id: null,
  name: null,
  email: null,
  body: null
};
